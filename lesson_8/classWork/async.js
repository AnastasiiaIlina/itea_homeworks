/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
const tableNode = document.getElementById('table');

async function tableRender() {
  const companiesResponse = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
  const companies = await companiesResponse.json();

  const companiesData = companies.map((item, index) => {
    const { company, balance, registered, address } = item;
    return {
      id: index + 1,
      company, 
      balance, 
      registered, 
      address: `${ address.country }, ${ address.city }, ${ address.street }, ${ address.house }`
    }
  });
  return companiesData;
}
const renderTable = (list) => {

  list.forEach(item => {
    const tr = document.createElement('tr');
    for(key in item) {
        const td = document.createElement('td');

        if(key === 'registered' || key === 'address') {
        
          const button = document.createElement('button');
          button.innerText = 'show';
          td.appendChild(button);
          
          const currentKey = key;

          button.addEventListener('click', () => {
            td.innerText = item[currentKey];
          });
        } else {
          td.innerHTML = item[key];
        } 
        tr.appendChild(td);
    }
   tableNode.appendChild(tr);
  });
};
const table = tableRender();
table.then( data => renderTable(data));
