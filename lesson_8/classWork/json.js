
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/

function renderData(e) { 
  e.preventDefault();

  const form = document.getElementById('form');
  const name = document.getElementById('name');
  const age = document.getElementById('age');
  const password = document.getElementById('password');
  const jsonField =  document.getElementById('json');

  const data = {
    name: name.value,
    age: age.value,
    password: password.value,
  }

  const dataJSON = JSON.stringify(data);
  console.log(dataJSON);
  console.log( JSON.parse(jsonField.value));
}

form.addEventListener('submit', renderData);
