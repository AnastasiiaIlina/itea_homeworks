/*
    172.17.13.236:3003/register/ -> POST
        {
            username: «»,
            email: «»
        }

    172.17.13.236:3003/task/ -> POST
        {
            title: «»,
            description: «»,
            user: «»
        }

    172.17.13.236:3003/users/ -> GET
    172.17.13.236:3003/tasks/ -> GET
*/

document.addEventListener('DOMContentLoaded', () => {
    fetch("http://172.17.13.236:3003/register/", {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ username: "Nastya", email: "test@gmail.com"}),
    })
    .then(res => res.json())
    .then(res => console.log(res) )
});

