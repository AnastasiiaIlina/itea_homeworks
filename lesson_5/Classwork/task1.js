/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

const train = {
    name: 'Train',
    speed: 140,
    passengers: 50,
    go: function(speed){
       this.speed = speed;
        return `Поезд ${ this.name } везет ${ this.passengers } пассажиров со скоростью ${ this.speed }`;
    } ,
    stop: function() {
        this.speed = 0;
        return `Поезд ${ this.name } остановился. Скорость ${ this.speed }`
    },
    pickUpPassengers: function(x) {
        this.passengers += x;
        return this.passengers 
    }
};

console.log(train);
console.log(train.go(150));
console.log(train.stop());
console.log(train.pickUpPassengers(10));
console.log(train.passengers);


