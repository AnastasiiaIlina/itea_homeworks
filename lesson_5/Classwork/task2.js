/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/

  let colors = {
    background: 'purple',
    color: 'white',
  };

  function myCall( color ){
    document.body.style.background = this.background;
    document.body.style.color = color;
  }
  myCall.call( colors, 'red' );

  // 1.2
  // function myBind(){
  //   document.body.style.background = this.background;
  //   document.body.style.color = this.color;
  // }

  // const func = myBind.bind(colors);
  // func();

  // 1.3
  // function myApply (title, color) {
  //   document.body.style.color = color;

  //   const h1 = document.createElement('h1');
  //   h1.innerText = title;
  //   document.body.appendChild(h1);
  // }

  // myApply.apply(colors, ['I know how binding works in JS', 'green'])
