/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства


    Dog {
      name: '',
      breed: '',
      status: 'idle',

      changeProp: function( key, value ){...},
      showProps: function(){...}
    }

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/

function AddDog(name, breed, status) {
  this.name = name;
  this.breed = breed;
  this.status = 'idle';
  this.showProps =  function (){
    for (key in this) {
      if( typeof(this[key]) !== 'function') {
        console.log(this[key]);
      }
    };
  };
  this.changeProp = function( key, value ) {
    this[key] = value;
  }
}

const dog = new AddDog('Bob', 'vv');
dog.changeProp('age', 2);
dog.showProps();