/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)
      decryptCesar1('Sdwq', 5);
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/

// зашифровывать
const decryptCarring = (shift, word) => {
  const decryptArray = [];
  let decryptWord = '';

  for (let char of word) {
    decryptArray.push(char.charCodeAt() + shift);
  }

  decryptWord = String.fromCharCode(...decryptArray);
  return decryptWord;
};

var decryptFunc = decryptCarring.bind(null, 3);
console.log(decryptFunc('word'));

// дешифровать
const encryptCarring = (shift, word) => {
  const encryptArray = [];
  let encryptWord = '';

  for (let char of word) {
    encryptArray.push(char.charCodeAt() - shift);
  }

  encryptWord = String.fromCharCode(...encryptArray);
  return  encryptWord;
}

var encryptFunc = encryptCarring.bind(null, 3);
console.log(encryptFunc('zrug'));