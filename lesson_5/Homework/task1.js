/*

  Задание:


    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);


    {
      avatarUrl: 'https://...',
      addLike: function(){...}
    }
    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }

      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>
*/


// 1
const Comment = function(name, message) {
  this.name = name;
  this.message = message;
  this.likes = 0;
};

Comment.prototype.addLike = function(){
  this.likes++;
  return this.likes;
};
Comment.prototype.avatarUrl = 'https://img.favpng.com/7/5/8/computer-icons-font-awesome-user-font-png-favpng-YMnbqNubA7zBmfa13MK8WdWs8.jpg';

const userComent1 = new Comment('Petya', 'I like JS!');
const userComent2 = new Comment('Kolya', 'I like PHP!');
const userComent3 = new Comment('Ivan', 'I like JAVA!');
const userComent4 = new Comment('Oleg', 'I like PYTHON');

userComent1.addLike()
userComent2.addLike();
userComent2.addLike();

// 2
var commentsArray = [userComent1,userComent2, userComent3, userComent4];
// console.log(CommentsArray);

// 3
const commentsFeed = document.getElementById('commentsFeed');

const renderComments = (commentsList) => {
  const template = ({ name, avatarUrl, likes, message }) =>
   `<div class="post">
      <div>  
        <h3 class="name">${ name } </h3>
        <img class="avatar" src = ${ avatarUrl } alt = 'user' />
      </div>

      <p>${ message }</p>
      <span class="likes">${ likes } лайк(-а, -ов) </span>
    </div>`;

  commentsList.forEach(item => commentsFeed.innerHTML += template(item));
}
renderComments(commentsArray);