
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
        + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
        и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
        Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

  const buttonsContainer = document.getElementById('buttonContainer');
  const buttons = document.querySelectorAll('.showButton');
  const tabs = Array.from(document.querySelectorAll('.tab'));

  buttons.forEach(button => {
    button.addEventListener('click', ()=> {
      const prevTab = document.querySelector('.tab.active');
      const activeTab = tabs.find(tab => (button.dataset.tab === tab.dataset.tab));

      Boolean(prevTab) && prevTab.classList.remove('active');
      activeTab.classList.add('active');
    });
  });

  const hideAllTabs = () => {
    buttons.forEach(item => {
      item.classList.remove('active');
    });

    tabs.forEach(item => {
      item.classList.remove('active');
    });
  };



