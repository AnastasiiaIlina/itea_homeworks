document.addEventListener("DOMContentLoaded", function() {
    const coursesContainer =  document.getElementById('courses-container');
    const coursesList = document.getElementById('courses-list');
    const coursesListCustomizer = document.getElementById('courses-list-customizer');
    const modal = document.querySelector('._courses-modal');
    const closeButton = document.getElementById('close-courses-button');
    const addButton = document.getElementById('add-courses-button');

    class Courses {
        constructor(){
            this.delete = this.delete.bind(this);
            this.close = this.close.bind(this);
            this.add = this.add.bind(this);
            this.render = this.render.bind(this);
            this.localStorage = this.localStorage.bind(this);
            this.savedRender = this.savedRender.bind(this);

            this.courses = [];
        }

        add() {
            const li = document.createElement('li');
            li.innerHTML =  `
                <input class="_courses-title" type="text"/>
                <input class="_courses-img" type="text" />
                <button class="_delete-button" type="button">X</button>
            `;

            coursesListCustomizer.appendChild(li);
            const deleteButton = li.querySelector('._delete-button');
            deleteButton.addEventListener('click', () => this.delete(li));
        }

        close() {
            coursesList.innerHTML = '';
            Array.from(coursesListCustomizer.children).forEach(item => {
                const li = document.createElement('li');
                li.innerHTML = `
                    <img src = ${ item.querySelector('._courses-img').value } alt = 'course' />
                    <h5>${ item.querySelector('._courses-title').value }</h5>
                `;
                coursesList.appendChild(li);

                this.courses.push({
                    title:item.querySelector('._courses-title').value, 
                    image: item.querySelector('._courses-img').value,
                });
            });
            this.localStorage();
            modal.classList.add('hidden');  

            if(!Boolean(coursesList.children.length)) {
                coursesList.innerText = 'Список курсов пуст';
            }
        }

        localStorage() {
            if(this.courses.length) {
                localStorage.setItem('coursesList', JSON.stringify(this.courses));
            } else {
                localStorage.removeItem('coursesList');
            }
        }

        savedRender() {
            const data = JSON.parse(localStorage.getItem('coursesList'));
            data.forEach(course => {
                const liCard = document.createElement('li');
                liCard.innerHTML = `
                    <img src = ${ course.image } alt = 'course' />
                    <h5>${ course.title }</h5>
                `;
                coursesList.appendChild(liCard);  
            
                const liControl = document.createElement('li');
                liControl.innerHTML =  `
                    <input class="_courses-title" type="text" value = "${ course.title }"/>
                    <input class="_courses-img" type="text" value = "${ course.image }"/>
                    <button class="_delete-button" type="button">X</button>
                `;

                coursesListCustomizer.appendChild(liControl);
                const deleteButton = liControl.querySelector('._delete-button');
                deleteButton.addEventListener('click', () => this.delete(liControl));
            });
        }

        render() {
            this.add();
        }

        delete(node) {
            coursesListCustomizer.removeChild(node);
        }

        init() {
            const editButton = document.querySelector('._edit-courses-button');

            coursesList.addEventListener('click', () => {
                modal.classList.remove('hidden');
            });

            if(localStorage.getItem('coursesList') !== null) {
                this.savedRender();
            }

            closeButton.addEventListener('click', this.close);
            addButton.addEventListener('click',  this.render);

            coursesContainer.addEventListener('mouseenter', () => {
                editButton.classList.remove('hidden');
            });
        
            coursesContainer.addEventListener('mouseleave', () => {
                editButton.classList.add('hidden');
            });
        
            editButton.addEventListener('click', () => {
               modal.classList.remove('hidden');
            });
        
        
            if(!Boolean(coursesList.children.length)) {
                coursesList.innerText = 'Список курсов пуст';
            }
        }
    }

    const coursesInstance = new Courses();
    coursesInstance.init();
});

