document.addEventListener("DOMContentLoaded", function() {
    const newsContainer =  document.querySelector('.news-container');
    const newsList = document.getElementById('news-list');
    const newsListCustomizer = document.getElementById('news-list-customizer');
    const editButton = newsContainer.querySelector('._edit-news-button');
    const modal = document.querySelector('._news-modal');
    const addButton = document.getElementById('add-news-button');
    const saveButton = document.getElementById('save-news-button');

    class News {
        constructor() {
            // date
            const today = new Date();
            const year = today.getFullYear() > 9 ? today.getFullYear() : '0' + today.getFullYear();
            const month = today.getMonth() > 8 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1);
            const date = today.getDate() > 9 ? today.getDate() : '0' + today.getDate();
            const dateTemplate = `${ date }/${ month }/${ year }`;

            this.id = today.getTime();
            this.date = dateTemplate;
            this.news = [];

            this.add = this.add.bind(this);
            this.save = this.save.bind(this);
            this.delete = this.delete.bind(this);
            this.localStorage = this.localStorage.bind(this);
            this.savedRender = this.savedRender.bind(this);
            this.render = this.render.bind(this);
        }

        add() {
            const li = document.createElement('li');
            li.innerHTML =  `
                <input class="_news-title" type="text" value="" />
                <input class="_news-content" type="text" value="" />
                <button class="_delete-button" type="button">X</button>
            `;
            newsListCustomizer.appendChild(li);

            const deleteButton = li.querySelector('._delete-button');
            deleteButton.addEventListener('click', () => this.delete(li));
        }

        delete(node) {
            newsListCustomizer.removeChild(node);
        }

        save() {
            this.news = [];
            newsList.innerHTML = '';
            newsListCustomizer.querySelectorAll('li').forEach(item => {
                const li = document.createElement('li');
                li.innerHTML =    `
                    <h2>${ item.querySelector('._news-title').value }</h2>
                    <span>${ this.date }</span>
                    <div class="news-content">${ item.querySelector('._news-content').value }</div>
                `;

                this.news.push({
                    title: item.querySelector('._news-title').value, 
                    content:item.querySelector('._news-content').value,
                    date: this.date,
                });

                newsList.appendChild(li);
            });

            this.localStorage();
            modal.classList.add('hidden');

            if(!Boolean(newsList.children.length)) {
                newsList.innerText = 'Список новостей пуст';
            }
        }

        localStorage() {
            if(this.news.length) {
                localStorage.setItem('newsList', JSON.stringify(this.news));
            } else {
                localStorage.removeItem('newsList');
            }
        }

        savedRender() {
            const data = JSON.parse(localStorage.getItem('newsList'));
            data.forEach(news => {
                const liCard = document.createElement('li');
                liCard.innerHTML = `
                    <h2>${ news.title }</h2>
                    <span>${ news.date }</span>
                    <div class="news-content">${ news.content }</div>
                `;
                newsList.appendChild(liCard);  
            
                const liControl = document.createElement('li');

                liControl.innerHTML =  `
                    <input class="_news-title" type="text" value="${ news.title }" />
                    <input class="_news-content" type="text" value="${ news.content }" />
                    <button class="_delete-button" type="button">X</button>
                `;

                newsListCustomizer.appendChild(liControl);
            
                const deleteButton = liControl.querySelector('._delete-button');
                deleteButton.addEventListener('click', () => this.delete(liControl));
            });
        }

        render() {
            this.add();
        }

        init(){
            if(localStorage.getItem('newsList') !== null) {
                this.savedRender();
            }

            addButton.addEventListener('click', this.add);
            saveButton.addEventListener('click', this.save);


            newsContainer.addEventListener('mouseenter', () => {
                editButton.classList.remove('hidden');
            });
        
            newsContainer.addEventListener('mouseleave', () => {
                editButton.classList.add('hidden');
            });
        
            editButton.addEventListener('click', () => {
               modal.classList.remove('hidden');
            });
        
        
            if(!Boolean(newsList.children.length)) {
                newsList.innerText = 'Список новостей пуст';
            }
        }
    }
    const newsInstance = new News();
    newsInstance.init();
});