document.addEventListener("DOMContentLoaded", function() {
   const fileRemote = document.getElementById('file-remote');
   const fileLocal = document.getElementById('file-local');
   const logo = document.querySelector('.logo > img');
   const logoContainer =  document.getElementById('logo-container');
   const modal = document.querySelector('._logo-modal');
   const logoButton =  document.getElementById('logo-button');
   const closeButton = document.getElementById('close-modal-button');
   const editButton = document.querySelector('._edit-logo-button');

   class Logo {
        constructor(nodeLogo, nodeLocal, nodeRemote){
            this.nodeLogo = nodeLogo;
            this.nodeLocal = nodeLocal;
            this.nodeRemote = nodeRemote;
            this.changeLogoLocal = this.changeLogoLocal.bind(this);
            this.changeLogoRemote = this.changeLogoRemote.bind(this);
        }

        changeLogoLocal() {
            const reader = new FileReader();

            reader.onload = (function(node) { 
                return function(e) { 
                    node.src = e.target.result; 
                    localStorage.setItem('logo', node.src);
                }; 
            })(this.nodeLogo);
           
            reader.readAsDataURL(this.nodeLocal.files[0]);
        }

        changeLogoRemote() {
            this.nodeLogo.src = this.nodeRemote.value;

            this.nodeLogo.onload = () => {
                localStorage.setItem('logo', this.nodeLogo.src);
                this.nodeRemote.value = '';
            }
            
            this.nodeLogo.onerror= () => {
                alert('Ошибка во время загрузки изображения');
                this.nodeRemote.value = '';
            }
        }

        init() {
            this.nodeLogo.src = localStorage.getItem('logo') || './img/logo.png';
            this.nodeLocal.addEventListener('change', this.changeLogoLocal);
            logoButton.addEventListener('click', this.changeLogoRemote);
        
            closeButton.addEventListener('click', () => {
                document.querySelector('._logo-modal.modal').classList.add('hidden');
            })

            logoContainer.addEventListener('mouseenter', () => {
                editButton.classList.remove('hidden');
            });
        
            logoContainer.addEventListener('mouseleave', () => {
                editButton.classList.add('hidden');
            });
        
            editButton.addEventListener('click', () => {
               modal.classList.remove('hidden');
            });
        }
    }

    const logoInstance = new Logo(logo, fileLocal, fileRemote);
    logoInstance.init();
});