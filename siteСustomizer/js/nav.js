document.addEventListener("DOMContentLoaded", function() {
    const nav = document.getElementById('nav');
    const navContainer = document.getElementById('nav-container');
    const navCustomizer = document.getElementById('nav-customizer');
    const saveButton = document.getElementById('save-nav-button');
    const addButton = document.getElementById('add-nav-button');
    const editButton = document.querySelector('._edit-nav-button');
    const modal = document.querySelector('._nav-modal');

    class Nav {
        constructor() {
            this.add = this.add.bind(this);
            this.save = this.save.bind(this);
            this.delete = this.delete.bind(this);
            this.localStorage = this.localStorage.bind(this);
            this.savedRender = this.savedRender.bind(this);
            this.render = this.render.bind(this);
            
            this.navItems = [];
            this.counter = 5;
        }

        add() {
           if(navCustomizer.children.length === this.counter) return;
            const li = document.createElement('li');

            li.innerHTML =  `
                <input class="_nav-title" type="text" value="" />
                <input class="_nav-link" type="text" value="" />
                <button class="_delete-button" type="button">X</button>
            `;
            navCustomizer.appendChild(li);

            const deleteButton = li.querySelector('._delete-button');
            deleteButton.addEventListener('click', () => this.delete(li));
        }

        save() {
            this.navItems = [];
            nav.innerHTML = '';
            navCustomizer.querySelectorAll('li').forEach(item => {
                const li = document.createElement('li');
                li.innerHTML =    `
                    <a href = ${ item.querySelector('._nav-link').value || '#empty' }>
                        ${ item.querySelector('._nav-title').value || 'empty' } 
                    </a>
                `;

                this.navItems.push({
                    title: item.querySelector('._nav-title').value, 
                    link: item.querySelector('._nav-link').value,
                });

                li.querySelector('a').addEventListener('click', (e) => {
                    e.preventDefault();
                });

                nav.appendChild(li);
            });

            if(!Boolean(nav.children.length)) {
                nav.innerText = 'Список новостей пуст';
            }

            this.localStorage();
            modal.classList.add('hidden');
        }

        delete(node) {
            navCustomizer.removeChild(node);
        }

        localStorage() {
            if(this.navItems.length) {
                localStorage.setItem('navItems', JSON.stringify(this.navItems));
            } else {
                localStorage.removeItem('navItems');
            }
        }

        savedRender() {
            const data = JSON.parse(localStorage.getItem('navItems'));
            data.forEach(item => {

                const liCard = document.createElement('li');
                liCard.innerHTML = `
                    <a href = ${ item.link || '#empty'}>
                        ${ item.title || 'empty'} 
                    </a>`;
                nav.appendChild(liCard);  
            
                const liControl = document.createElement('li');

                liControl.innerHTML =  `
                    <input class="_nav-title" type="text" value="${ item.title }" />
                    <input class="_nav-link" type="text" value="${ item.link }" />
                    <button class="_delete-button" type="button">X</button>
                `;

                navCustomizer.appendChild(liControl);
            
                const deleteButton = liControl.querySelector('._delete-button');
                deleteButton.addEventListener('click', () => this.delete(liControl));

                liCard.querySelector('a').addEventListener('click', (e) => {
                    e.preventDefault();
                });
            });
        }

        render() {
            this.add();
        }

        init() {
            if(localStorage.getItem('navItems') !== null) {
                this.savedRender();
            }
            saveButton.addEventListener('click', this.save);
            addButton.addEventListener('click',  this.add);

            navContainer.addEventListener('mouseenter', () => {
                editButton.classList.remove('hidden');
            });
        
            navContainer.addEventListener('mouseleave', () => {
                editButton.classList.add('hidden');
            });
        
            editButton.addEventListener('click', () => {
               modal.classList.remove('hidden');
            });
        
        
            if(!Boolean(nav.children.length)) {
                nav.innerText = 'Список новостей пуст';
            }
        }
    }

    const navInstance = new Nav();
    navInstance.init();
});
   