

    /*

        Документация:
        
        https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
        
        form.checkValidity() > Проверка всех полей формы на валидость
        form.reportValidity() > Проверяет все поля на валидность и выводит возле каждого из не прошедшего валидацию
            сообщение с ошибкой

        formElement.validity > Объект с параметрами валидности поля 
        formElement.setCustomValidity(message) > Метод который задаст validity.valid = false, и при попытке отправки
            сообщения выведет message в браузерный попал

        Классы для стилизации состояния элемента
        input:valid{}
        input:invalid{}

        Задание:
        Используя браузерное API для валидации форм реализовать валидацию следующей формы:

        - Имя пользователя: type:text -> validation: required; minlength = 2;  
            Если пустое выввести сообщение: "Как тебя зовут дружище?!"
        - Email: type: email -> validation: required; minlength = 3; validEmail;
            Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
        - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
            Если пустой вывести сообщение: "Я никому не скажу наш секрет";
        - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
            Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
        - Напиши спасибо за яблоки: type: text -> validation: required; 
            Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

        - Согласен на обучение: type: checkbox -> validation: required;
            Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

        Внизу две кнопки:

        1) Обычный submit который отправит форму, если она валидна.
        2) Кнопка Validate(Проверить) которая запускает методы:
            - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
            - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

    */

    const form = document.getElementById('form');
    const submitButton =  document.getElementById('submitButton');
    const validateButton = document.getElementById('validateButton');
    const result = document.getElementById('result');

    const name = document.getElementById('name');
    const email = document.getElementById('email');
    const password = document.getElementById('password');
    const applesNumber =  document.getElementById('applesNumber');
    const thanks = document.getElementById('thanks');
    const agreement = document.getElementById('agreement');

    name.addEventListener('input', () => {
        if(name.validity.valueMissing) { 
            name.setCustomValidity('Как тебя зовут дружище?!');
        } else if(name.validity.tooShort) {
            name.setCustomValidity('Слишком короткое имя');
        } else {
            name.setCustomValidity('');
        }
    });
        
    email.addEventListener('input', () => {
        console.log(email.validity);
        if(email.validity.valid) {
            email.setCustomValidity('');
        } else {
            email.setCustomValidity('Ну и зря, не получишь бандероль с яблоками!');
        }
    });

    password.addEventListener('input', () => {
        if(password.validity.valueMissing) {
            password.setCustomValidity('Я никому не скажу наш секрет');
        } else {
            password.setCustomValidity('');
        }
    });

    applesNumber.addEventListener('input', () => {
        if(Number(applesNumber.value) === 0) {
            applesNumber.setCustomValidity('Ну хоть покушай немного... Яблочки вкусные');
        } else if(Number(applesNumber.value) < 0) {
            applesNumber.setCustomValidity('Число не может быть отрицательным');
        } else {
            applesNumber.setCustomValidity('');
        }
    });

    thanks.addEventListener('input', () => {
        if(thanks.value.toLowerCase() !== 'спасибо') {
            thanks.setCustomValidity('Фу, неблагодарный(-ая)!');
        } else {
            thanks.setCustomValidity('');
        }
    });

    agreement.addEventListener('change', () => {
        if(!agreement.checked) {
            agreement.setCustomValidity('Необразованные живут дольше! Хорошо подумай!');
        } else {
            agreement.setCustomValidity('');
        }
    }); 
    
    validateButton.addEventListener('click', () => {
        const validity =  form.checkValidity();

        if(validity) {
            result.innerHTML = 'Форма заполнена!';
            e.preventDefault();
        } else {
            form.reportValidity(); 
            result.innerHTML =  'Не валидная форма!'
        }
    });




    



